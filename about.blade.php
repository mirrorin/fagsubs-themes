@extends(theme_view('layout'))

@section('title')
  About Me - {{ site_title() }}
@stop

@section('content')
    <section class="home">
    	<div class="container">
            <div class="row">
                <div class="col-md-12">
    				<h3>About me</h3>
    				<hr>	
						<p><span class="label label-danger">Q</span>
						 Who am I?</p>
						<p><span class="label label-success">A</span>
						 Just ordinary people ヽ(‘ ∇‘ )ノ</p>
				</div>
			</div>
		</div>
	</section>

@stop
