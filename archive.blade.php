@extends(theme_view('layout'))

@section('title')
Archives
@stop

@section('content')
    <section class="home">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    {{-- Archive Heading --}}
                    {{-- Notice the triple brackets to escape any xss for tags and search term. --}}
                    @if (isset($tag))
                    <h3 class="title">{{{ ucfirst($tag) }}} Archives</h3>
                    @elseif ($search)
                    <h3 class="title">Results for {{{ $search }}}</h3>
                    @else
                    <h3 class="title">Archives</h3>
                    <hr>
                    @endif

                    @foreach ($posts as $post)
                     <ul>
                        <li>
                            <span>
                                <i class="fa fa-calendar">&nbsp;</i>{{ date("d-M-Y", strtotime($post['publish_date'])) }}</span> - <a href="{{ wardrobe_url('/post/'.$post['slug']) }}">{{ $post['title'] }}</a>
                            </span>
                        </li>
                    </ul>                   
                    @endforeach
                    

                </div>
            </div>
        </div>

    </section>
    {{ $posts->links() }}
@stop
