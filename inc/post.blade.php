<div class="post">
    @if(Request::is('posts'))
        <h2><a href="{{ wardrobe_url('posts/'.$post->slug) }}">{{ $post->title }}</a></h2>
    @elseif(Request::is('posts*'))
        <h1>{{ $post->title }}</h1>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <table class="table">
                <tr>
                    <td class="min-width">Date:</td>
                    <td>{{ date("d-M-Y", strtotime($post->publish_date)) }}</td>
                </tr>
                @if($post->tags->first()->tag != '')
                <tr>
                    <td class="min-width">Tags:</td>
                    <td>
                        @include(theme_view('inc.tags'))
                    </td>
                </tr>
                @endif
            </table>
        </div>
    </div>
    <div class="content">
      {{ $post->parsed_content }}
    </div>

    <a id="gotop" href="#">   
        <span>▲</span> 
    </a>
    
    <!-- AddThis Smart Layers BEGIN -->
    <!-- Go to http://www.addthis.com/get/smart-layers to customize -->
    <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-535e539e54531802">
    </script>
    <script type="text/javascript">
        addthis.layers({
        'theme' : 'transparent',
        'share' : {
        'position' : 'left',
        'numPreferredServices' : 4
        }   
    });
    </script>
    <!-- AddThis Smart Layers END -->
</div>