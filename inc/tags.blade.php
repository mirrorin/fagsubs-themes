<ul class="tags">
  @foreach ($post->tags as $item)
    @if ($item->tag != "")
      <li><a  class="label label-info" href="{{ wardrobe_url('/tag/'.$item->tag) }}"><i class="fa fa-tags"></i> {{ $item->tag }}</a></li>
    @endif
  @endforeach
</ul>
