@extends(theme_view('layout'))

@section('title')
  {{ site_title() }}
@stop

@section('content')
    <section class="home">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Recent Posts</h3>
                    <hr>
                    <table class="table">
                    @foreach ($posts as $post)
                    <tr>
                        <td class="no-padding-left"><i class="fa fa-file-text"></i> <a href="{{ wardrobe_url('posts/'.$post->slug) }}">{{ $post->title }}</a></td>
                        <td class="no-padding-right text-right hidden-xs">{{ date("d-M-Y", strtotime($post->publish_date)) }}</td>
                    </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </div>
	</section>
    {{ $posts->links() }}
@stop
