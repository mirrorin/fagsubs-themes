<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="//fagsubs.pw/img/favicon.jpg">

    <!-- Bootstrap Core CSS -->
    @if(Config::get('wardrobe-themes::theme') == 'fagsubs')
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    @else
        <link href="//netdna.bootstrapcdn.com/bootswatch/3.1.1/flatly/bootstrap.min.css" rel="stylesheet">
    @endif

    <!-- Font Awesome CSS -->
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="{{ asset(theme_path('css/style.css')) }}" rel="stylesheet" media="screen">

</head>
<body>
    <div class="page-wrapper">
        <div class="navbar-xs">
            <div class="navbar-primary">
                <div class="navbar navbar-default navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toogle="collapse" data-target="navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <a href="{{ wardrobe_url('/') }}" class="navbar-brand"><i class="fa fa-home"></i> {{ site_title() }}</a>
                            
                        </div>

                        <div class="collapse navbar-collapse" role="navigation">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="/archive"><i class="fa fa-archive"></i> Archives</a></li>
                                <li><a href="/projects"><i class="fa fa-flask"></i> Projects</a></li>
                                <li><a href="/about"><i class="fa fa-user"></i> About</a></li>
                                <li><a href="/rss"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            @yield('content')
        </div>
        
    </div>


    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Powered by <a href="#">Laravel</a></p>
                </div>
            </div>
        </div>
    </footer>

        @if(!Request::is('post/preview*'))
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"><\/script>')</script>
        @endif
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script src="{{ asset(theme_path('js/script.js')) }}"></script>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.pagination').find('ul').addClass('pagination pagination-sm');
        });
        </script>
</body>
</html>