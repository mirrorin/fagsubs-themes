@extends(theme_view('layout'))

@section('title')
  Projects
@stop

@section('content')
    <section class="work">
    	<div class="container">
	        <h1>Work</h1>
	        <hr>
	        <div class="row">
	            <div class="col-xs-6 col-md-4">
	                <div class="thumbnail">
	                    <img src="/img/sample.gif" alt="">
	                    <div class="caption">
	                        <h3>Thumbnail Caption</h3>
	                        <p>Something here</p>
	                    </div>
	                </div>
	            </div>
	            <div class="col-xs-6 col-md-4">
	                <div class="thumbnail">
	                    <img src="/img/sample.gif" alt="">
	                    <div class="caption">
	                        <h3>Thumbnail Caption</h3>
	                        <p>Something here</p>
	                    </div>
	                </div>
	            </div>
	            <div class="col-xs-6 col-md-4">
	                <div class="thumbnail">
	                    <img src="/img/sample.gif" alt="">
	                    <div class="caption">
	                        <h3>Thumbnail Caption</h3>
	                        <p>Something here</p>
	                    </div>
	                </div>
	            </div>
	            <div class="col-xs-6 col-md-4">
	                <div class="thumbnail">
	                    <img src="/img/sample.gif" alt="">
	                    <div class="caption">
	                        <h3>Thumbnail Caption</h3>
	                        <p>Something here</p>
	                    </div>
	                </div>
	            </div>
	            <div class="col-xs-6 col-md-4">
	                <div class="thumbnail">
	                    <img src="/img/sample.gif" alt="">
	                    <div class="caption">
	                        <h3>Thumbnail Caption</h3>
	                        <p>Something here</p>
	                    </div>
	                </div>
	            </div>
	            <div class="col-xs-6 col-md-4">
	                <div class="thumbnail">
	                    <img src="/img/sample.gif" alt="">
	                    <div class="caption">
	                        <h3>Thumbnail Caption</h3>
	                        <p>Something here</p>
	                    </div>
	                </div>
	            </div>
	        </div>
    	</div>
    </section>
@stop